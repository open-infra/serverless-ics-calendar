const AWS = require('aws-sdk');
const ical = require('ical-generator');
const { DateTime } = require("luxon");


// Set the AWS region and the name of the DynamoDB table
const region = 'ap-southeast-2';
const tableName = 'sls-calendar-prototype-table';

// Create a DynamoDB client
const dynamoDb = new AWS.DynamoDB.DocumentClient({ region });

// Define the handler function for the /calendar route
exports.calendar = async (event, context) => {
  try {
    // Query the DynamoDB table for all event records
    const params = {
      TableName: tableName,
      KeyConditionExpression: 'objectType = :objectType',
      ExpressionAttributeValues: {
        ':objectType': 'event'
      }
    };
    const result = await dynamoDb.query(params).promise();

    // Create an ICS calendar file with the event records
    const calendar = ical({
      // domain: 'example.com',
      name: 'Test Calendar',
      timezone: 'UTC' //TODO add to platform config file
    });

    //DateTime.fromISO
    console.log(`[i]\tIterating over new events...`)
    result.Items.forEach(event => {

      const eventPayload = {
        start: DateTime.fromISO(event.start).toJSDate(),
        end: DateTime.fromISO(event.end).toJSDate(),
        summary: event.summary,
        description: "",
        location: ""
      }

      calendar.createEvent(eventPayload);
    });
    console.log()
    const icsFile = calendar.toString();

    // Return the ICS calendar file as a download
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/calendar',
        'Content-Disposition': 'attachment; filename=calendar.ics'
      },
      body: icsFile
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: 'Error generating ICS calendar file'
    };
  }
};

// Define the handler function for the /api/calendar route
exports.addEvents = async (event, context) => {
  // Parse the request body
  const requestBody = JSON.parse(event.body);
  const { passphrase, events } = requestBody;

  try {
    // Query the DynamoDB table for the config record
    const params = {
      TableName: tableName,
      KeyConditionExpression: 'objectType = :objectType and creationTimestamp = :creationTimestamp',
      ExpressionAttributeValues: {
        ':objectType': 'config',
        ':creationTimestamp': 0
      }
    };
    const result = await dynamoDb.query(params).promise();

    // If the config record does not exist, return a 400 response
    if (result.Items.length === 0) {
      return {
        statusCode: 400,
        body: 'passphrase not set'
      };
    }

    // Check that the passphrase in the request matches the passphrase in the config record
    if (result.Items[0].passphrase !== passphrase) {
      return {
        statusCode: 400,
        body: 'incorrect passphrase'
      };
    }

    // Validate the event records
    const missingParameters = [];
    events.forEach((event, index) => {
      if (!event.summary || !event.start || !event.end) {
        missingParameters.push(index);
      }
    });

    if (missingParameters.length > 0) {
      return {
        statusCode: 400,
        body: `Event ${missingParameters.join(', ')} is missing at least one of the following parameters: summary, start, end`
      };
    }

    // Save the events to the DynamoDB table
    const putRequests = events.map(event => ({
      PutRequest: {
        Item: {
          objectType: 'event',
          creationTimestamp: Date.now(),
          ...event
        }
      }
    }));
    const newRecordParams = {
      RequestItems: {
        [tableName]: putRequests
      }
    };
    await dynamoDb.batchWrite(newRecordParams).promise();

    return {
      statusCode: 200,
      body: 'success'
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: 'Error saving events'
    };
  }
};


  exports.setPassphrase = async (event, context) => {
    // Parse the request body
    console.log(`[i]\tSetting passphrase`)
    const requestBody = JSON.parse(event.body);
    const { passphrase } = requestBody;
  
    try {
      // Query the DynamoDB table for the config record
      const configCheckParams = {
        TableName: tableName,
        KeyConditionExpression: 'objectType = :objectType and creationTimestamp = :creationTimestamp',
        ExpressionAttributeValues: {
          ':objectType': 'config',
          ':creationTimestamp': 0
        }
      };;
      const result = await dynamoDb.query(configCheckParams).promise();
  
      // If the config record already exists, return a 400 response
      if (result.Items.length > 0) {
        return {
          statusCode: 400,
          body: JSON.stringify({error:'passphrase already set'})
        };
      }
  
      // Save the passphrase to the DynamoDB table
      const params = {
        TableName: tableName,
        Item: {
          objectType: 'config',
          creationTimestamp: 0,
          passphrase
        }
      };
      await dynamoDb.put(params).promise();
      console.log(`[i]\t\tPassphrase set`);
      return {
        statusCode: 200,
        body: 'success'
      };
    } catch (error) {
      console.log(`[i]\tPassphrase NOT set, error to follow:`)
      console.error(error);
      return {
        statusCode: 500,
        body: 'Error saving passphrase'
      };
    }
  };
  