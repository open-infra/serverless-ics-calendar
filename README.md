# serverless-ics-calendar

A serverless application for managing an internet-served ICS calendar!

- Easy to add events
- Easy to share to other people

## Getting started

Firstly, install the dependencies:

`npm install`

Next, configure the `service` name and `TableName` variables in the `serverless.yml` file.

Run `serverless deploy`.

If no issues occur during deployment, set a passphrase using a command similarly structured to the cURL request below. The passphrase will be used to authenticate users adding new events, and the request URL should reflect the URL you received in the ouput from the previous deployment command.

```
curl --request POST \
  --url https://YOUR_GATEWAY_SUBDOMAIN.execute-api.YOUR_REGION.amazonaws.com/dev/api/calendar/config \
  --header 'Content-Type: application/json' \
  --data '{
	"passphrase": "YOUR_PASSPHRASE"
}'
```

To add an event, modify this request as needed (please note that you can add multiple events at once, _however_ if one event creation fails then the request will return an error response).

```
curl --request POST \
  --url https://YOUR_GATEWAY_SUBDOMAIN.execute-api.YOUR_REGION.amazonaws.com/dev/api/calendar/ \
  --header 'Content-Type: application/json' \
  --data '{
  "passphrase": "YOUR_PASSPHRASE",
  "events": [
    {
      "summary": "EVENT TEST",
      "start": "2022-12-27T19:00:00+09:30",
      "end": "2022-12-27T19:15:00+09:30"
    }
  ]
}'
```
